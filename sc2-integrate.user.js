// ==UserScript==
// @name            SC2-integrate
// @license         MIT
// @version         7.0.0
// @downloadURL     https://gitlab.com/stream-cinema-community/db-checker/raw/master/sc2-integrate.user.js
// @updateURL       https://gitlab.com/stream-cinema-community/db-checker/raw/master/sc2-integrate.user.js
// @description     Integrace SCC do CSFD, TMDB, TRAKT.TV and Webshare
// @match           https://webshare.cz/*
// @match           https://www.csfd.cz/*
// @match           https://www.csfd.sk/*
// @match           https://trakt.tv/shows/*
// @match           https://trakt.tv/movies/*
// @match           https://www.themoviedb.org/tv/*
// @match           https://www.themoviedb.org/movie/*
// ==/UserScript==

const ICONS_ROOT = "https://gitlab.com/stream-cinema-community/db-checker/raw/master/icons/";

// icons for CSFD page
const ICON_CSFD_GREY = ICONS_ROOT + "logoCSFDGrey.png"
const ICON_CSFD_BLUE = ICONS_ROOT + "logoCSFDBlue.png"
const ICON_CSFD_RED  = ICONS_ROOT + "logoCSFDRed.png"
const ICON_VLC  = ICONS_ROOT + "logoVLC.png"


// icon for CSFD lists
const ICON_CSFD_LIST = ICONS_ROOT + "logoCSFDList.png"

// icon for Trakt page
const ICON_TRAKT = ICONS_ROOT + "logoTrakt.png"

// Ymovie link 
const YMOVIE_LINK = "https://ymovie.streamcinema.cz/#/scc/"

var indexStart = -1;
var indexEnd = -1;
var parentEl;
var childEl;

var br;

// Inserts newly created node after the node - this is not standart function
function insertAfter(newNode, referenceNode) {
    referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
}

function getServiceURL(service, type, id) {
    return "https://plugin.sc2.zone/api/media/filter/v2/service?type=" + type + "&service=" + service + "&value=" + id + "&access_token=9ajdu4xyn1ig8nxsodr3"
    
}

function getInfoFromResponse(res) {

    var streams = res.available_streams;
    var streamsCount;
    if(streams.count > 0){
        streamsCount = streams.count;
    } else {
        streamsCount = '-';
    }
    var langs = '';
    if(streams.languages.audio.map.length > 0) {
        var i;
        for(i = 0; i < streams.languages.audio.map.length; i++) {
            if(langs != '') {
                langs = langs + ', ' + streams.languages.audio.map[i].toUpperCase();
            } else {
                langs = streams.languages.audio.map[i].toUpperCase();    
            }
        }
    } else {
        langs = '-';
    }
    var subs = '';
    if(streams.languages.subtitles.map.length > 0) {
        for(i = 0; i < streams.languages.subtitles.map.length; i++) {
            if(subs != '') {
                subs = subs + ', ' + streams.languages.subtitles.map[i].toUpperCase();
            } else {
                subs = streams.languages.subtitles.map[i].toUpperCase();    
            }
        }
    } else {
        subs = '-';
    }

    return 'Streamu: ' + streamsCount + '\nAudio: ' + langs + '\nTitulky: ' + subs;
}

function getTraktURLFromResponse(res) {
    var data = res.hits.hits[0]._source;
    var slug = data.services.slug;
    var id = data.services.trakt;
    var mediaType = data.info_labels.mediatype;
    if(slug) {
        if (mediaType == 'movie' || mediaType == 'tvshow') {
          var traktType;
          if(mediaType == 'movie') {
              traktType = 'movies';
          } else {
              traktType = 'shows';
          }
          return 'https://trakt.tv/' + traktType + '/' + slug;
        }
    } else {
        var url = 'https://trakt.tv/search/trakt/' + id + '?id_type=' + mediaType;
        return url;
    }
}

function getCSFDLogoByRating(r) {
    // get rating and set correct color of logo
    var ratingText = '';
    if (r) {
        ratingText = r;
    } else {
        var ratingsEl = document.getElementsByClassName('rating-average')[0];
        if(!ratingsEl) {
            ratingsEl = document.getElementsByClassName('film-rating-average')[0];
        }
        if(!ratingsEl) {
            ratingsEl = document.getElementsByClassName('rating-average rating-average-withtabs')[0];
        }
        ratingText = ratingsEl.innerHTML;
    }
    // console.log('[SCC] rating = ' + ratingText);
    var rating = parseInt(ratingText.substring(0, ratingText.indexOf("%")));
    var sc2Src;
    if(rating < 30) {
        sc2Src = ICON_CSFD_GREY;
    } else if(rating < 70) {
        sc2Src = ICON_CSFD_BLUE;
    } else {
        sc2Src = ICON_CSFD_RED;
    }
    return sc2Src;
}

function getCSFDLink(traktURL, sc2Src, infoText) {
    // create a link node
    var link = document.createElement('a');
    link.href = traktURL;

    var sc2 = document.createElement('img');
    sc2.src = sc2Src;
    sc2.setAttribute('width', '176px');
    sc2.setAttribute('style', 'margin: 4px 0 2px 0;');

    sc2.title = infoText;

    // append logo tolink node
    link.appendChild(sc2);

    return link;
}

function checkMediaCSFD(id) {
    var xhttp = new XMLHttpRequest();
    var url = getServiceURL('csfd', '*', id);
    // console.log('[SCC] url = ' + url);
    // console.log('[SCC] CSFD id = ' + id);
    xhttp.open("GET", url, true);
    xhttp.send();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            // parse response...
            var res = JSON.parse(this.responseText);
            if(res.totalCount == 0) {
                return;
            }
            var data = res.hits.hits[0];
            var source = res.hits.hits[0]._source;
            var mediatype = source.info_labels.mediatype;
            console.log('mediatype = ' + mediatype);
            var infoText = getInfoFromResponse(source);
            var traktURL = getTraktURLFromResponse(res);

            var sc2Src = getCSFDLogoByRating();
            var link = getCSFDLink(traktURL, sc2Src, infoText);

            br = document.createElement('br');

            var ymlink = document.createElement('a');
            var ymlinkhref = "";
            if(mediatype == "movie") {
                ymlinkhref = YMOVIE_LINK + "movie/" + data._id;
            } else if (mediatype == "tvshow") {
                ymlinkhref = YMOVIE_LINK + "series/" + data._id;
            } else if (mediatype == "season") {
                ymlinkhref = YMOVIE_LINK + "season/" + data._id;
            } else {
                ymlinkhref = YMOVIE_LINK + "episode/" + data._id;
            }
            // console.log('[SCC: Ymovie link: %o]', ymlinkhref);
            ymlink.href = ymlinkhref;
            ymlink.innerHTML = "<strong>YMovie " + mediatype + "</strong>";

            var el = document.getElementsByClassName('tabs tabs-rating rating-fan-switch')[0];
            
            var divEl = document.createElement("div");
            divEl.setAttribute("style", "text-align: center;");

            divEl.appendChild(link);
            divEl.appendChild(br);
            divEl.appendChild(ymlink);

            el.insertBefore(divEl, el.childNodes[1]);
        }
    };
}
function checkCSFDList(url, movieList) {
    var xhttp = new XMLHttpRequest();
    xhttp.open("GET", url, true);
    xhttp.send();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            // parse response...
            var data = (JSON.parse(this.responseText)).hits.hits;
            var mapData = new Map();
            var csfdid;
            var infoText;
            for(let i = 0; i < data.length; i++) {
                var source = data[i]._source;
                csfdid = source.services.csfd;
                infoText = getInfoFromResponse(source);
                mapData.set(csfdid, infoText)
            }

            for(let i = 0; i < movieList.length; i++) {
                var item = movieList[i];
                var el = item.childNodes[1].childNodes[1];
                var title = el.innerHTML;
                var link = el.getAttribute('href');
                indexStart = link.indexOf('film/') + 5;
                indexEnd = link.indexOf('-');
                var id = link.substring(indexStart, indexEnd);
                if(mapData.has(id)) {
                    var rating = item.childNodes[3].childNodes[9].childNodes[1].innerHTML;
                    var sc2src = getCSFDLogoByRating(rating);
                    var sc2 = document.createElement('img');
                    sc2.src = sc2src;
                    sc2.setAttribute('width', '116px');
                    sc2.setAttribute('style', 'margin: 2px;');
                    sc2.title = mapData.get(id);
                    // link.appendChild(sc2);
                    item.childNodes[3].childNodes[9].insertBefore(sc2, item.childNodes[3].childNodes[9].childNodes[3]);
                }
            }


        }
    };

}

function checkCSFDListSearch(url, movieList) {
    var xhttp = new XMLHttpRequest();
    xhttp.open("GET", url, true);
    xhttp.send();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            // parse response...
            var data = (JSON.parse(this.responseText)).hits.hits;
            var mapData = new Map();
            var csfdid;
            var infoText;
            for(let i = 0; i < data.length; i++) {
                var source = data[i]._source;
                csfdid = source.services.csfd;
                infoText = getInfoFromResponse(source);
                mapData.set(csfdid, infoText)
            }

            for(let i = 0; i < movieList.length; i++) {
                var item = movieList[i];
                var el = item.childNodes[1].childNodes[1];
                var title = el.innerHTML;
                var link = el.getAttribute('href');
                indexStart = link.indexOf('film/') + 5;
                indexEnd = link.indexOf('-');
                var id = link.substring(indexStart, indexEnd);
                if(mapData.has(id)) {
                    var sc2src = ICON_CSFD_LIST;
                    var sc2 = document.createElement('img');
                    sc2.src = sc2src;
                    sc2.setAttribute('width', '14px');
                    sc2.setAttribute('style', 'margin: 0 2px 6px 0;');
                    sc2.title = mapData.get(id);
                    item.childNodes[3].childNodes[1].insertBefore(sc2, item.childNodes[3].childNodes[1].childNodes[1]);
                }
            }


        }
    };

}

function checkCSFDListHome(url, movieList) {
    var xhttp = new XMLHttpRequest();
    xhttp.open("GET", url, true);
    xhttp.send();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            // parse response...
            var data = (JSON.parse(this.responseText)).hits.hits;
            var mapData = new Map();
            var csfdid;
            var infoText;
            for(let i = 0; i < data.length; i++) {
                var source = data[i]._source;
                csfdid = source.services.csfd;
                infoText = getInfoFromResponse(source);
                mapData.set(csfdid, infoText)
            }

            for(let i = 0; i < movieList.length; i++) {
                var item = movieList[i];
                var el = item.childNodes[1];
                var title = el.innerHTML;
                var link = el.getAttribute('href');
                indexStart = link.indexOf('film/') + 5;
                indexEnd = link.indexOf('-');
                var id = link.substring(indexStart, indexEnd);
                if(mapData.has(id)) {
                    var sc2src = ICON_CSFD_LIST;
                    var sc2 = document.createElement('img');
                    sc2.src = sc2src;
                    sc2.setAttribute('width', '14px');
                    sc2.setAttribute('style', 'margin: 0 2px 6px 0;');
                    sc2.title = mapData.get(id);
                    el.parentNode.insertBefore(sc2, el);
                }
            }


        }
    };

}

function checkMediaTrakt(type, id) {
    var xhttp = new XMLHttpRequest();
    var url = getServiceURL('trakt_with_type', type, id);
    // console.log('[SCC] url: ' + url);
    xhttp.open("GET", url, true);
    xhttp.send();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            // parse response...
            var res = JSON.parse(this.responseText);
            if(res.totalCount == 0) {
                return;
            }
            // console.log('[SCC] response: ' + res);
            var data = res.hits.hits[0];
            var source = res.hits.hits[0]._source;
            var infoText = getInfoFromResponse(source);

            var parentEl;
            parentEl = document.getElementsByClassName('sidebar affixable affix-top')[0];
            //if(!parentEl) {
            //    parentEl = document.getElementsByClassName('sidebar affixable affix-top')[0];
            //}
            console.log('[SCC] parent el: ' + parentEl);
            childEl = parentEl.childNodes[1];

            var sc2 = document.createElement('img');
            sc2.src = ICON_TRAKT;
            sc2.title = infoText
            sc2.setAttribute('width', '173px');
            sc2.setAttribute('style', 'margin-top: 8px; margin-bottom: 8px;');

            br = document.createElement('br');

            var ymlink = document.createElement('a');
            ymlink.setAttribute('style', 'color: gray;');
            // ymlink.setAttribute('style', 'text-decoration: none;');
            
            var ymlinkhref = "";
            if(source.info_labels.mediatype == "movie") {
                ymlinkhref = YMOVIE_LINK + "movie/" + data._id;
            } else if (source.info_labels.mediatype == "tvshow") {
                ymlinkhref = YMOVIE_LINK + "series/" + data._id;
            } 
            ymlink.href = ymlinkhref;
            ymlink.innerHTML = "<strong>YMovie</strong>";

            var divEl = document.createElement("div");
            // divEl.setAttribute("style", "text-align: center;");
            divEl.setAttribute("style", "padding: 0px 0px 10px 0px;");

            divEl.appendChild(sc2);
            divEl.appendChild(br);
            divEl.appendChild(ymlink);


            parentEl.insertBefore(divEl, childEl);
        }
    };
}

async function checkMediaTMDB(id, slug, type) {
    var xhttp = new XMLHttpRequest();
    var xhttp2 = new XMLHttpRequest();
    var requestOK = false;
    
    xhttp2.open("GET", getServiceURL('tmdb', type, id), true);
    xhttp2.send();
    xhttp2.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            // parse response...
            var res = JSON.parse(this.responseText);
            if(res.totalCount == 0) {
                return;
            }
            var data = res.hits.hits[0];
            var source = res.hits.hits[0]._source;
            var mediatype = source.info_labels.mediatype;
            console.log('mediatype = ' + mediatype);

            var infoText = getInfoFromResponse(source);
            var traktURL = getTraktURLFromResponse(res);

            // create a link node if does not exist
            var scclink = document.getElementById('scclogo');
            if(requestOK == false) {
                var link = document.createElement('a');
                link.id = 'scclogo';
                link.href = traktURL;
    
                var sc2 = document.createElement('img');
                sc2.title = infoText;
                sc2.src = ICON_TRAKT;
                sc2.setAttribute('width', '184px');
                sc2.setAttribute('style', 'margin-bottom: 12px;');
    
                // append logo to link node
                link.appendChild(sc2);

                br = document.createElement('br');
    
                var ymlink = document.createElement('a');
                var ymlinkhref = "";
                if(source.info_labels.mediatype == "movie") {
                    ymlinkhref = YMOVIE_LINK + "movie/" + data._id;
                } else if (source.info_labels.mediatype == "tvshow") {
                    ymlinkhref = YMOVIE_LINK + "series/" + data._id;
                } 
                ymlink.href = ymlinkhref;
                ymlink.innerHTML = "<strong>YMovie</strong>";
    
                var divEl = document.createElement("div");
                // divEl.setAttribute("style", "text-align: center;");
                divEl.setAttribute("style", "padding: 0px 0px 10px 0px;");
    
                divEl.appendChild(link);
                divEl.appendChild(br);
                divEl.appendChild(ymlink);

                var afterElement = document.getElementsByClassName('facts left_column')[0].firstChild;
                insertAfter(divEl, afterElement);
            }

        }
    };
}

function sc2Integrate() {

    var href = window.location.href;
    if ((href == 'https://www.csfd.cz/') || (href == 'https://www.csfd.sk/')) {
        // csfd.cz and csfd.sk home page
        console.log('[SCC] CSFD CZ SK home page');
        var movieList = document.getElementsByClassName('film-title');
        var url = "https://plugin.sc2.zone/api/media/filter/v2/service?type=*&service=csfd"
        // console.log('[SCC: movie list %o]', movieList);
        for(let i = 0; i < movieList.length; i++) {
            var item = movieList[i];
            var el = item.childNodes[1];
            // console.log('[SCC: el %o]', el);
            var title = el.innerHTML;
            var link = el.getAttribute('href');
            indexStart = link.indexOf('film/') + 5;
            indexEnd = link.indexOf('-');
            var id = link.substring(indexStart, indexEnd);
            url = url + "&value=" + id;
        }
        url = url + "&access_token=9ajdu4xyn1ig8nxsodr3"
        checkCSFDListHome(url, movieList);
        
    
    } else if ((href.indexOf('www.csfd.cz/film/') > 0) || (href.indexOf('www.csfd.sk/film/') > 0)) {
        // Movie or TV show page
        console.log('[SCC] CSFD CZ SK movie or TV show page');
        var csfdId;
        var count = href.split("/").length;
        // console.log('[SCC] count = ' + count);
        if(count > 7) {
            // season or episode
            var filmIndex = href.indexOf('film/') + 5;
            indexStart = href.indexOf("/", filmIndex) + 1;
            indexEnd = href.indexOf("-", indexStart);
            csfdId = href.substring(indexStart, indexEnd);
        } else {
            // movie or root tv show page
            indexStart = href.indexOf('film/') + 5;
            href = href.substring(indexStart);
            if(href.indexOf("-serie-") < 0) {
                indexStart = 0;
            } else {
                indexStart = href.indexOf("/") + 1;
            }
            indexEnd = href.indexOf('-', indexStart);
            csfdId = href.substring(indexStart, indexEnd);
        }
        checkMediaCSFD(csfdId);

    } else if ((href.indexOf('csfd.cz/hledat/') > 0) || (href.indexOf('csfd.sk/hladat/') > 0)) {
        // csfd.cz - search
        console.log('[SCC] CSFD CZ SK search page');
        movieList = document.getElementsByClassName('article article-poster-60');
        url = "https://plugin.sc2.zone/api/media/filter/v2/service?type=*&service=csfd"
        // console.log('[SCC: movie list %o]', movieList);
        for(let i = 0; i < movieList.length; i++) {
            item = movieList[i];
            el = item.childNodes[1].childNodes[1];
            // console.log('[SCC: el %o]', el);
            title = el.innerHTML;
            link = el.getAttribute('href');
            indexStart = link.indexOf('film/') + 5;
            indexEnd = link.indexOf('-');
            id = link.substring(indexStart, indexEnd);
            url = url + "&value=" + id;
        }
        url = url + "&access_token=9ajdu4xyn1ig8nxsodr3"
        checkCSFDListSearch(url, movieList);

    } else if ((href.indexOf('csfd.cz/zebricky/') > 0) || (href.indexOf('csfd.sk/rebricky/') > 0)) {
        // csfd.cz - rankings
        console.log('[SCC] CSFD CZ SK rankings page');
        movieList = document.getElementsByClassName('article article-poster-60');
        url = "https://plugin.sc2.zone/api/media/filter/v2/service?type=*&service=csfd"
        // console.log('[SCC: movie list %o]', movieList);
        for(let i = 0; i < movieList.length; i++) {
            item = movieList[i];
            el = item.childNodes[1].childNodes[1];
            // console.log('[SCC: el %o]', el);
            title = el.innerHTML;
            link = el.getAttribute('href');
            indexStart = link.indexOf('film/') + 5;
            indexEnd = link.indexOf('-');
            id = link.substring(indexStart, indexEnd);
            url = url + "&value=" + id;
        }
        url = url + "&limit=300&access_token=9ajdu4xyn1ig8nxsodr3"
        checkCSFDList(url, movieList);

    } else if (href.indexOf('trakt.tv') > 0) {
        // trakt.tv
        console.log('[SCC] Trakt.tv');
        var info = document.getElementsByClassName('summary-user-rating')[0];

        if (info) {
            type = info.getAttribute("data-type");
            // console.log('SCC type %o', type)
            id = '';
            if(type == 'movie') {
                id = 'movie:' + info.getAttribute("data-movie-id")
            } else {
                type = 'tvshow';
                id = 'tvshow:' + info.getAttribute("data-show-id")
            }
            // console.log('SCC Trakt id %o', id)
            checkMediaTrakt(type, id);
            
            return;
        }
    
    } else if (href.indexOf('themoviedb.org') > 0) {
        var slug = '';
        var type = 'movie'
        var tmdbId = ''
        // TMDB movie
        if (href.indexOf('/movie/') > 0) {
            console.log('[SCC] TMDB movie page');
            indexStart = href.indexOf('-') + 1;
            if(indexStart > 0) {
                slug = href.substring(indexStart);
            }
            indexStart = href.lastIndexOf('/') + 1;
            indexEnd = href.indexOf('-');
            if(indexEnd > 0) {
                tmdbId = href.substring(indexStart, indexEnd);
            } else {
                tmdbId = href.substring(indexStart);
            }
            checkMediaTMDB(tmdbId, slug, type);
        // TMDB TV show
        } else {
            console.log('[SCC] TMDB TV show page'); 
            type = 'tvshow';
            if (href.indexOf('/season/') > 0) {
                console.log('[SCC] TMDB TV show page - season with list of episodes'); 
            } else if (href.indexOf('/seasons') > 0) {
                console.log('[SCC] TMDB TV show page - list of seasons'); 
            } else {
                console.log('[SCC] TMDB TV show page - main'); 
                indexStart = href.indexOf('-') + 1;
                if(indexStart > 0) {
                    slug = href.substring(indexStart);
                }
                indexStart = href.lastIndexOf('/') + 1;
                indexEnd = href.indexOf('-');
                if(indexEnd > 0) {
                    tmdbId = href.substring(indexStart, indexEnd);
                } else {
                    tmdbId = href.substring(indexStart);
                }
                console.log('[SCC] TMDB id: %o, slug: %o, type: %o', tmdbId, slug, type);
                checkMediaTMDB(tmdbId, slug, type);
            }
        }

    } else if (href.indexOf('webshare.cz/#/file/') > 0) {
        console.log('[SCC] Webshare');
        setTimeout(ws, 2000);
    }

}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
}


function ws () {
    var href = window.location.href;

    var element = document.getElementsByClassName('ws-control-panel');
    if(element.length > 0) {
        var wst = getCookie('wst');
        // console.log('[SCC: WS token: %o]', wst);
        var ident = href.substring(href.indexOf('/file/') + 6)
        if(ident.indexOf('/') > 0) {
            ident = ident.substring(0, ident.indexOf('/'))
        }
        // console.log('[SCC: WS ident.1: %o]', ident);
        
        var data = new FormData();
        data.append('ident', ident);
        data.append('wst', wst);
        var xhr = new XMLHttpRequest();
        xhr.open('POST', 'https://webshare.cz/api/file_link/');
        xhr.onload = function () {
            // do something to response
            var res = this.responseText;
            // console.log(res);
            var startInd = res.indexOf('<link>') + 6;
            var endInd = res.indexOf('</link>');
            var url = res.substring(startInd, endInd);
            // console.log('[SCC: WS file link: %o]', url);
            
            var link = document.createElement('a');
            link.id = 'vlclogo';
            link.href = 'vlc://' + url;

            var vlc = document.createElement('img');
            vlc.title = 'Play stream in VLC';
            vlc.src = ICON_VLC;
            vlc.setAttribute('width', '40px');
            vlc.setAttribute('height', '40px');
            vlc.setAttribute('style', 'margin: 4px 0 0 2px;');
            
            // append logo to link node
            link.appendChild(vlc);

            var afterElement = element[0].childNodes[1];
            insertAfter(link, afterElement);

        };
        xhr.send(data);
    }
    
}

sc2Integrate();